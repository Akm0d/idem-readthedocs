<!--
*** This was build starting with the following README template:
*** https://github.com/othneildrew/Best-README-Template
*** This project uses a .md rather than .rst file as PyPi supports markdown
*** and is rendered better at the local GitLab/GitHub repository of the
*** source code.
-->

<!--
*** Using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
[![Idem][idem-shield]][idem-url]

<!-- PROJECT LOGO -->
<div align="center">
<br />
<p align="center">
  <a href="https://gitlab.com/saltstack/pop/idem-readthedocs">
    <img src="docs/_static/images/idem-readthedocs-logo.png" alt="Logo" width="500">
  </a>

  <h3 align="center"><b>idem-readthedocs</b></h3>

  <p align="center">
    Extends <b><a href="https://idem.readthedocs.io/">idem</a></b> to interact with <b><a href="https://readthedocs.org/">readthedocs.org</a></b>, making
    <br />
    management of hosted documentation projects easy!
    <br />
    <a href="https://idem-readthedocs.readthedocs.io"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/saltstack/pop/idem-readthedocs/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/saltstack/pop/idem-readthedocs/issues">Request Feature</a>
  </p>
</p>
</div>

<!-- TABLE OF CONTENTS -->
## Table of Contents

<!-- TOC -->

- [Table of Contents](#table-of-contents)
- [About The Project](#about-the-project)
    - [Built With](#built-with)
- [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Installation](#installation)
        - [Install from source](#install-from-source)
- [Usage](#usage)
    - [Project Builds](#project-builds)
        - [Examples](#examples)
- [Roadmap](#roadmap)
- [Contributing](#contributing)
- [Acknowledgements](#acknowledgements)

<!-- /TOC -->


<!-- ABOUT THE PROJECT -->
## About The Project



### Built With

* [idem](https://idem.readthedocs.io/) is an idempotent, imperatively executed,
  declarative programming language written in Python. This project extends
  idem!
* [Plugin Oriented Programming (POP)](https://pop.readthedocs.io/)
  * For more information, reference
    [Intro to Plugin Oriented Programming](https://pop-book.readthedocs.io/)

## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

- Python 3.6+
- git _(if installing from source, or contributing to the project)_

### Installation

> _**NOTE**: If wanting to contribute to the project, and setup your local
development environment, see the [Contributing](#contributing) section for more
information._

If wanting to use `idem-readthedocs`, you can do so by either installing from
PyPI or from source.

#### Install from source

Requires the following installed:

- git
- Python 3.6+

```bash
# clone repo
git clone git@gitlab.com:saltstack/pop/idem-readthedocs.git
cd idem-readthedocs

# Setup venv
python3 -m venv .venv
source .venv/bin/activate
pip install -e .
```

## Usage

`idem-readthedocs` includes exec and acct plugins for `idem`.

### Project Builds

Builds are created by Read the Docs whenever a `Project` has its documentation
built. Frequently this happens automatically via a web hook but can be triggered
manually. `idem-readthedocs` allows for programmatic in an ad-hoc fashion, or as
part of automated CI/CD pipelines.

Builds can be viewed in the build page on readthedocs.org for a project. For
example, here is [Pip’s build page](https://readthedocs.org/projects/pip/builds/).
See [Build Process](https://docs.readthedocs.io/en/stable/builds.html) for more
information.

> _Reference the [Read the Docs API v3 on Builds](https://docs.readthedocs.io/en/stable/api/v3.html#builds)_
_for more information._

#### Examples

**readthedocs.build.list**

```bash
# List builds for a target docs project
idem exec readthedocs.build.list <rtd-api-key> <project-name>
```

**readthedocs.build.get**

```bash
# Get information from a particular build of a target docs project
idem exec readthedocs.build.get <rtd-api-key> <project-name> <build-id>
```

**readthedocs.build.trigger**

```bash
# Trigger a new build for a particular version of a target docs project
idem exec readthedocs.build.trigger <rtd-api-key> <project-name> <version>
```

## Roadmap

Reference the [open issues](https://gitlab.com/saltstack/pop/idem-readthedocs/issues) for a list of proposed features (and known issues).

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b AmazingFeature`)
3. Setup your local development environment

   ```bash
   # setup venv
   python3 -m venv .venv
   source .venv/bin/activate
   pip install -r requirements.txt
   pip install -r requirements-test.txt

   # pre-commit configuration
   pre-commit install
   pre-commit autoupdate
   ```

4. Hack away!
5. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
6. Push to the Branch (`git push origin AmazingFeature`)
7. Open a Pull Request


## Acknowledgements

* [ReadTheDocs.org](https://readthedocs.org) for hosting and automated hosting
  of documentation in the open-source community
* [Img Shields](https://shields.io) for making repository badges easy

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[idem-shield]: https://img.shields.io/badge/made%20with-Idem-blue
[idem-url]: https://idem.readthedocs.io/
