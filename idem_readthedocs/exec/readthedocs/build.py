# -*- coding: utf-8 -*-
"""
Builds API
https://docs.readthedocs.io/en/stable/api/v3.html#builds
"""

__func_alias__ = {"list_": "list"}


async def list_(hub, ctx):
    """
    List builds for a project

    API docs: https://docs.readthedocs.io/en/stable/api/v3.html#builds-listing
    """
    # Prep REST API endpoint
    url = f"{ctx['acct']['endpoint_url']}/{ctx['acct']['project_name']}/builds"

    """
    # TODO: Add additional, supported arguments
    # Check for supported query parameters
    if commit or running:
        url += '?'
        if commit:
            url += f"commit={commit}"
            if running:
                url += f"&running={running}"
        elif running:
            url += f"running={running}"
    else:
        url += "/"
    """

    # REST API call
    api_response = await ctx["acct"]["session"].get(url)
    return hub.tool.readthedocs.helpers.check_api_response(ctx, api_response)


async def get(hub, ctx, build_id):
    """
    Get details about a particular build in a project

    API docs: https://docs.readthedocs.io/en/stable/api/v3.html#build-details
    """
    # Prep REST API endpoint
    url = (
        f"{ctx['acct']['endpoint_url']}/{ctx['acct']['project_name']}/builds/{build_id}"
    )

    """
    # TODO: Add additional, supported arguments
    # Check for supported query parameters
    if expand == 'config':
        url += '?expand=config'
        # TODO 'expand' is only allowed to have one possible value, if present.
        # Error out if incorrect value
    """

    # REST API call
    api_response = await ctx["acct"]["session"].get(url)
    return hub.tool.readthedocs.helpers.check_api_response(ctx, api_response)


async def trigger(hub, ctx, version="latest"):
    """
    Trigger a new build of a project

    API docs: https://docs.readthedocs.io/en/stable/api/v3.html#build-triggering
    """
    # Prep REST API endpoint
    url = f"{ctx['acct']['endpoint_url']}/{ctx['acct']['project_name']}/versions/{version}/builds/"

    # REST API call
    api_response = await ctx["acct"]["session"].get(url)
    return hub.tool.readthedocs.helpers.check_api_response(ctx, api_response)
